const usuario = require('../../models/usuario')
const bicicleta = require('../../models/bicicleta')
const reserva = require('../../models/reserva')
const mongoose = require('mongoose')


describe('Probando Usuarios', function (){
    beforeEach(function (done){
        const mongoDB = "mongodb://localhost/test"
        mongoose.connect(mongoDB, { useNewUrlParser: true })

        const db = mongoose.connection
        db.on('Error', console.error.bind(console, 'Connection error'))
        db.once('Open', function (){
            console.log("Estamos conectados al test de la base de datos")
            done()
        })
    })
    afterEach(function (done){
        reserva.deleteMany({}, function (err, success){
            if (err) console.log(err)
            usuario.deleteMany({}, function (err, success){
                if (err) console.log(err)
                bicicleta.deleteMany({}, function (err, success){
                    if (err) console.log(err)
                    done()
                })
            })
        })
    })

    describe('Cuando un usuarios reserva una bicicleta', () =>  {
        it('debe de existir la reserva', (done) => {
            const user = new usuario({nombre: 'Gerardo Castillo'})
            user.save()
            const bicycle = new bicicleta({code: 23, color: "cristiano", modelo: "urbana"})
            bicycle.save()

            let hoy = new Date()
            let mannana = new Date()
            mannana.setDate(hoy.getDate()+1)
            user.reservar(bicycle.id, hoy, mannana, function (err, reser){
                reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas){
                    console.log(reservas[0])
                    expect(reservas.length).toBe(1)
                    expect(reservas[0].diasDeReserva()).toBe(2)
                    expect(reservas[0].bicicleta.code).toBe(23)
                    expect(reservas[0].usuario.nombre).toBe(user.nombre)
                    done()
                })
            })

        })
    })




})